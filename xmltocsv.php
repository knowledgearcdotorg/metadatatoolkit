<?php
require_once "vendor/autoload.php";

use League\Csv\Writer;

$reader = new XMLReader;
$reader->open("file:///home/haydenyoung/Desktop/test.xml");

$columnMappings =
    [
        "inm:CatID"=>"dc.identifier",
        "inm:CatClassification"=>"dc.identifier.govdoc",
        "inm:CatTitle"=>"dc.title",
        "inm:CatSubtitle"=>"dc.title",
        "inm:CatAuthor"=>"dc.contributor.author",
        "inm:CatCorporateAuthor"=>"dc.contributor.author",
        "inm:CatEditor"=>"dc.contributor.editor",
        "inm:CatSource"=>"dc.source",
        "inm:CatPublisher"=>"dc.publisher",
        "inm:CatDatePublished"=>"dc.date.issued",
        "inm:CatPhysDesc"=>"dc.format",
        "inm:CatSeries"=>"dc.relation.ispartofseries",
        "inm:CatSubjects"=>"dc.subject",
        "inm:CatAbstract"=>"dc.description.abstract",
        "inm:CatLanguage"=>"dc.language",
        "inm:CatRecordType"=>"dc.type",
        "inm:CatURL"=>"files"
	];

$columns = [];
$records = [];

$i = 0;

while ($reader->read()) {
    if ($reader->name == 'inm:Record' && $reader->nodeType == XMLReader::ELEMENT) {
        $doc = new DOMDocument;

        $doc->appendChild($doc->importNode($reader->expand(), true));

        foreach ($doc->firstChild->childNodes as $node) {
            $name = $node->nodeName;
            $value = trim($node->nodeValue);

            if (isset($columnMappings[$name])) {
                if ($node->nodeType == XML_ELEMENT_NODE) {
                    if (array_search($columnMappings[$name], $columns) === false) {
                        $columns[] = $columnMappings[$name];
                        $records[$i][$columnMappings[$name]] = [];
                    }

                    if (!empty($value)) {
                        $records[$i][$columnMappings[$name]][] = $value;
                    }
                }
            }
        }

        $i++;
    }
}

$writer = Writer::createFromPath('/tmp/file.csv', 'w+');
$writer->insertOne($columns);

foreach ($records as $record) {
    $row = [];

    foreach ($columns as $column) {
        if (isset($record[$column])) {
            if ($column == 'dc.title') {
                $divider = ': ';
            } else {
                $divider = '||||';
            }

            $row[] = implode($divider, $record[$column]);
        } else {
            $row[] = null;
        }
    }

    $writer->insertOne($row);
}
