<?php
require_once "vendor/autoload.php";

use League\Csv\Reader;
use League\Csv\Statement;

if ($argc !== 5) {
    echo "Usage: controlledvocabularybuilder.php csv xml root-element-name root-element-desc\n";
    exit();
}

// pop the command.
array_shift($argv);

$csv = Reader::createFromPath(array_shift($argv), 'r');

$xml = array_shift($argv);

$rootElementName = array_shift($argv);

// @TODO Maybe make this optional
$rootElementDesc = array_shift($argv);

$csv->setHeaderOffset(0); //set the CSV header offset
$stmt = (new Statement());

$records = $stmt->process($csv);

$dom = new DomDocument();
$root = $dom->appendChild($dom->createElement("node"));

$idAttr = $dom->createAttribute("id");
$idAttr->value = $rootElementName;

$root->appendChild($idAttr);

$labelAttr = $dom->createAttribute("label");
$labelAttr->value = $rootElementDesc;

$root->appendChild($labelAttr);

$tree = array();

foreach ($records as $record) {
    $node = array();

    foreach (array_reverse(array_filter($record)) as $field) {
        $node = array($field=>$node);
    }

    $tree = array_merge_recursive($tree, $node);
}

parseTree($tree, $root);

function parseTree($tree, $parentXmlNode) {
    $dom = $parentXmlNode->ownerDocument;

    $isComposedByXmlNode = $dom->createElement("isComposedBy");
    $parentXmlNode->appendChild($isComposedByXmlNode);

    $parentId = $parentXmlNode->getAttribute('id');

    $i = 0;

    foreach (array_keys($tree) as $key) {
        $xmlNode = $dom->createElement("node");

        // id attribute.
        $idAttr = $dom->createAttribute("id");
        $idAttr->value = $parentId.($i+1);
        $xmlNode->appendChild($idAttr);

        // label attribute.
        $labelAttr = $dom->createAttribute("label");
        $labelAttr->value = htmlspecialchars($key);
        $xmlNode->appendChild($labelAttr);

        $isComposedByXmlNode->appendChild($xmlNode);

        if (count($tree[$key])) {
            parseTree($tree[$key], $xmlNode);
        }

        $i++;
    }
}

$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;

file_put_contents($xml, $dom->saveXml($root));
