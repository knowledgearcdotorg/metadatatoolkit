<?php
/**
 * Created by PhpStorm.
 * User: haris
 * Date: 1/9/19
 * Time: 1:28 AM
 */


$dir = new RecursiveDirectoryIterator($argv[1]);
$persons = file_get_contents($argv[1] . '/Persons.js');
$persons = json_decode($persons, true);
$persons = $persons['response']['docs'];
foreach (new RecursiveIteratorIterator($dir) as $filename => $file) {
    $file_parts = pathinfo($filename);
    if ($file_parts['extension'] == 'js' && $file_parts['filename'] != 'Persons' && $file_parts['filename'] != 'Person_additional') {
        $json_object = file_get_contents($filename);

        echo "Parsing " . $filename . "\n";

        $name = $file_parts['filename'];
        $path = $file_parts['dirname'];
        handleCases($json_object, $name, $path, $persons);
    }
}

function handleCases($json_object, $name, $path, $persons)
{
    writeToCsv($json_object, $name, $path, $persons, "NIBR, PublikasjonerNIBR, Notat_NIBR", "NIBR_PublikasjonerNIBR_Notat_NIBR");
    writeToCsv($json_object, $name, $path, $persons, "NIBR, PublikasjonerNIBR, \u00c5rsrapport_NIBR", "NIBR_PublikasjonerNIBR_u00c5rsrapport_NIBR");
    writeToCsv($json_object, $name, $path, $persons, "NIBR, Rapport_NIBR, PublikasjonerNIBR", "NIBR_Rapport_NIBR_PublikasjonerNIBR");
}


function writeToCsv($json_object, $name, $path, $persons, $attr_eztag_lk, $route)
{
    $data = json_decode($json_object, true);
    $data2 = json_decode($json_object, true);
    $response = [];
    $columns = ['dc.title[en]', 'dc.description.abstract[en]', 'dc.description[en]', 'dc.title[no_NB]', 'dc.description.abstract[no_NB]', 'dc.description[no_NB]', 'dc.contributor.author', 'fagarkivet.author.link', 'dc.date.issued', 'dc.identifier.issn',
        'dc.identifier.isbn', 'dc.relation.ispartofseries',
        'fagarkivet.source.pagenumber', 'dc.type', 'dc.subject', 'dc.publisher', 'files'];

    $parsed = [];

    foreach ($data as $key => $datum) {
        if (isset($datum["attr_eztag_lk"]) && $datum["attr_eztag_lk"] == $attr_eztag_lk) {
            $new_data = [];
            foreach ($columns as $column) {
                $new_data[$column] = '';
            }

            if (array_search($datum['meta_id_si'], $parsed) === false) {
                $lang = ($datum['meta_language_code_ms'] == 'eng-GB' ? 'en' : 'no_NB');

                $translationFound = false;

                while (($datum2 = current($data2)) !== false && !$translationFound) {
                    if ($datum['meta_id_si'] == $datum2['meta_id_si'] && $datum['meta_language_code_ms'] != $datum2['meta_language_code_ms']) {
                        $translationFound = true;

                        $lang2 = ($datum2['meta_language_code_ms'] == 'eng-GB' ? 'en' : 'no_NB');

                        $new_data['dc.title[' . $lang2 . ']'] = $datum2['attr_main_title_s'] ?? '';
                        $new_data['dc.description.abstract[' . $lang2 . ']'] = trim(str_replace(PHP_EOL, ' ', ($datum2['attr_short_description_t'] ?? '')), ' ');
                        $new_data['dc.description[' . $lang2 . ']'] = trim(str_replace(PHP_EOL, ' ', ($datum2['attr_summary_t'] ?? '')), ' ');
                    }

                    next($data2);
                }

                reset($data2);

                $new_data['dc.title[' . $lang . ']'] = $datum['attr_main_title_s'] ?? '';
                $new_data['dc.description.abstract[' . $lang . ']'] = trim(str_replace(PHP_EOL, ' ', ($datum['attr_short_description_t'] ?? '')), ' ');
                $new_data['dc.description[' . $lang . ']'] = trim(str_replace(PHP_EOL, ' ', ($datum['attr_summary_t'] ?? '')), ' ');

                $new_data['dc.contributor.author'] = '';
                $new_data['fagarkivet.author.link'] = '';

                if (isset($datum['attr_authors_t'])) {
                    $authors = explode(PHP_EOL, $datum['attr_authors_t']);
                    foreach ($authors as $author) {
                        $flagC = 0;
                        $authorFlag = '';
                        while (($person = current($persons)) !== false && $flagC == 0) {
                            if ($person['meta_guid_ms'] == $author || $person['full_name_s'] == $author) {
                                $flagC = 1;
                                $new_data['dc.contributor.author'] .= str_replace(array('\n', '\r'), '', $person['last_name_s'] . ', ' . $person['first_name_t'] . '||||');
                                if ($person['full_name_s'] == $author) {
                                    $new_data['fagarkivet.author.link'] .= str_replace(array('\n', '\r'), '', $person['last_name_s'] . ', ' . $person['first_name_t']) . ';' . str_replace(array('\n', '\r'), '', 'https://ansatt.oslomet.no/user/' . $person['meta_guid_ms'] . '||||');
                                } else {
                                    $new_data['fagarkivet.author.link'] .= str_replace(array('\n', '\r'), '', $person['last_name_s'] . ', ' . $person['first_name_t']) . ';' . str_replace(array('\n', '\r'), '', 'https://ansatt.oslomet.no/user/' . $author . '||||');
                                }
                            }


                            next($persons);
                        }

                        reset($persons);

                        if ($flagC == 0) {
                            $new_data['dc.contributor.author'] .= str_replace(array('\n', '\r'), '', $author . '||||');
                        }
                    }
                }

                $new_data['dc.contributor.author'] = rtrim($new_data['dc.contributor.author'], '||||');
                $new_data['dc.contributor.author'] = implode('', preg_split('/\r\n|\r|\n/', $new_data['dc.contributor.author']));
                $new_data['fagarkivet.author.link'] = rtrim($new_data['fagarkivet.author.link'], '||||');

                $new_data['dc.date.issued'] = $datum['attr_publishing_year_t'] ?? '';
                $new_data['dc.identifier.issn'] = $datum['attr_issn_t'] ?? '';
                $new_data['dc.identifier.isbn'] = $datum['attr_isbn_t'] ?? '';
                $new_data['dc.relation.ispartofseries'] = $datum['attr_series_title_t'] ?? '';
                $new_data['fagarkivet.source.pagenumber'] = $datum['attr_total_pages_t'] ?? '';
                $new_data['dc.type'] = $datum['attr_publication_type_t'] ?? '';
                if (isset($datum['ezf_df_tags'])) {
                    $new_data['dc.subject'] = implode(',', $datum['ezf_df_tags']);
                } else {
                    $new_data['dc.subject'] = '';
                }
                $new_data['dc.publisher'] = $datum['attr_published_t'] ?? '';
                $new_data['files'] = ($datum['fileName'] ?? '') . "||||" . ($datum['imageName'] ?? '');
                $new_data['files'] = rtrim($new_data['files'], '||||');
                $response[] = $new_data;
            }

            $parsed[] = $datum['meta_id_si'];
        }
    }
    $outputPath = $path . '/' . $route;
    if (!file_exists($outputPath)) {
        mkdir($outputPath, 0777, true);
    }
    $outputCSV = $path . '/' . $route . '/' . $name . '.csv';
    $f = fopen($outputCSV, 'w');
    $columns = array_unique($columns);
    fputcsv($f, $columns);
    foreach ($response as $resp)
        fputcsv($f, $resp);

    return;

}
