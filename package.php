<?php
require_once "vendor/autoload.php";

use League\Csv\Reader;
use League\Csv\Statement;

if ($argc !== 4) {
    echo "Usage: package.php csv source destination\n";
    exit();
}

$dest = array_pop($argv);

$src = array_pop($argv);

if (!is_dir($src)) {
    echo $src . " is not a valid source path.";
    exit();
}

$csv = Reader::createFromPath(array_pop($argv), 'r');

$csv->setHeaderOffset(0); //set the CSV header offset
$stmt = (new Statement());

$records = $stmt->process($csv);

if (!is_dir($dest)) {
    mkdir($dest);
} else {
    deleteDir($dest);
    mkdir($dest);
}

$i = 0;

foreach ($records as $record) {
    $domDocs = [];
    $doc = 'dublin_core';
    $domDocs[$doc] = new DomDocument();;
    $dcDefault = $domDocs[$doc]->appendChild($domDocs[$doc]->createElement($doc));
    $dc = $dcDefault;
    $temp = '';
    $path = $dest . $i;
    $isFile = false;
    mkdir($path);

    foreach ($record as $key => $value) {
        $parts = explode(".", $key);

        $values = explode("||||", $value);

        if (count($parts) < 2) {
            if (strtolower($parts[0]) == 'files') {
                $files = $values;
                $fileCount = 0;
                foreach ($files as $file) {
                    $file = trim($file);
                    if (!is_file($src . "/" . $file)) {
                        echo $file . " cannot be found or is not a valid file, skipping..." . PHP_EOL;
                    } else {
                        $fileOut = pathinfo($file, PATHINFO_BASENAME);
                        copy($src . "/" . $file, $path . "/" . $fileOut);
                        file_put_contents($path . "/contents", $fileOut . "\n", FILE_APPEND);
                        $fileCount++;
                    }
                }
                if ($fileCount > 0) {
                    $isFile = true;
                }
            }
        } else {
            foreach ($values as $v) {
                if ($parts[0] != 'dc') {
                    $doc = 'metadata_' . $parts[0];
                    if ($parts[0] != $temp) {
                        $temp = $parts[0];
                        $domDocs[$doc] = new DomDocument();
                        $schemaAttr = $domDocs[$doc]->createAttribute("schema");
                        $schemaAttr->value = $parts[0];
                        $dcOther = $domDocs[$doc]->appendChild($domDocs[$doc]->createElement("dublin_core"));
                    }
                    $dc = $dcOther;
                } else {
                    $doc = "dublin_core";
                    $dc = $dcDefault;
                }

                if (isset($schemaAttr) && !isset($dc->schema)) {
                    $dc->appendChild($schemaAttr);
                }

                $dcValue = $dc->appendChild($domDocs[$doc]->createElement("dcvalue"));

                $nameAttr = $domDocs[$doc]->createAttribute("element");
                $scanAttr = explode("[", $parts[1]);
                $nameAttr->value = $scanAttr[0];
                if (count($scanAttr) > 1) {
                    $scanAttr = trim($scanAttr[1], "]");
                    $languageAttr = $domDocs[$doc]->createAttribute("language");
                    $languageAttr->value = $scanAttr;
                }
                $dcValue->appendChild($nameAttr);

                $valueAttr = $domDocs[$doc]->createAttribute("qualifier");

                if (count($parts) == 3) {
                    $langScan = explode("[", $parts[2]);
                    $valueAttr->value = $langScan[0];
                    if (count($langScan) > 1 && !isset($languageAttr)) {
                        $langScan = trim($langScan[1], "]");
                        $languageAttr = $domDocs[$doc]->createAttribute("language");
                        $languageAttr->value = $langScan;
                    }

                } else {
                    $valueAttr->value = 'none';
                }

                $dcValue->appendChild($valueAttr);
                if (isset($languageAttr)) {
                    $dcValue->appendChild($languageAttr);
                    unset($languageAttr);
                }
                $dcValue->appendChild($domDocs[$doc]->createTextNode($v));
            }
        }

        unset($schemaAttr);
    }

    if ($isFile != true) {
        unset($domDocs);
    }
    if (isset($domDocs)) {
        foreach ($domDocs as $ki => $document) {
            $document->save($path . "/" . $ki . ".xml");
        }
    }
    $i++;
}

RemoveEmptySubFolders($dest);

function deleteDir($dirPath)
{
    if (!is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function RemoveEmptySubFolders($path)
{
    $empty=true;
    foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file)
    {
        $empty &= is_dir($file) && RemoveEmptySubFolders($file);
    }
    return $empty && rmdir($path);
}
/*
create xml package area

FOREACH records AS record
    create record area under package
    create xml recordset

    FOREACH record->fields AS field
        IF field is not file field THEN
            add field to recordset
        ELSE
            files = explode filelist by comma

            FOREACH files AS file
                add file to record area
            ENDFOREACH
        ENDIF
    ENDFOREACH

    write xml recordset to package file
ENDFOREACH*/
